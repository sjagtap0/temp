import React, { useState, useEffect} from 'react';
import Amplify, { API, graphqlOperation, Auth, Hub } from 'aws-amplify';
import AWSAppSyncClient, { AUTH_TYPE } from 'aws-appsync';
import * as AmazonCognitoIdentity from 'amazon-cognito-identity-js';
import gql from 'graphql-tag';
import logo from './logo.svg';
import './App.css';
import { listTodos } from './graphql/queries';
import { createTodo } from './graphql/mutations';

const client = new AWSAppSyncClient({
  url: "https://relbel433zhwdnvoskbgbbqwny.appsync-api.ap-south-1.amazonaws.com/graphql",
  region: "ap-south-1",
  auth: {
    type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
    jwtToken: async () => (await Auth.currentSession()).getIdToken().getJwtToken(),
  },
});

// Amplify.configure({
//   "aws_project_region": "ap-south-1",
//   "aws_appsync_graphqlEndpoint": "https://relbel433zhwdnvoskbgbbqwny.appsync-api.ap-south-1.amazonaws.com/graphql",
//   "aws_appsync_region": "ap-south-1",
//   "aws_appsync_authenticationType": "API_KEY",
//   "aws_appsync_apiKey": "da2-pkaf4mq6uzbthb73fh6shrafbm",
//   "aws_cognito_identity_pool_id": "ap-south-1:634fe15e-1f61-45f2-b42c-a809e4340c82",
//   "aws_cognito_region": "ap-south-1",
//   "aws_user_pools_id": "ap-south-1_dKzSLzDJN",
//   "aws_user_pools_web_client_id": "668v5b6rq4h7e33al764nmso8t",
//   "oauth": {
//     "domain": "amplifyappa17d8077-a17d8077-dev.auth.ap-south-1.amazoncognito.com",
//     "scope": [
//         "email",
//     ],
//     "redirectSignIn": "http://localhost:3000/",
//     "redirectSignOut": "http://localhost:3000/",
//   },
//   "social": {
//     'FB': '660901318152990'
//   },
//   "federationTarget": "COGNITO_USER_POOLS",
// });


// Amplify.configure({
//   "aws_project_region": "ap-south-1",
//   "aws_appsync_graphqlEndpoint": "https://relbel433zhwdnvoskbgbbqwny.appsync-api.ap-south-1.amazonaws.com/graphql",
//   "aws_appsync_region": "ap-south-1",
//   "aws_appsync_authenticationType": "API_KEY",
//   "aws_appsync_apiKey": "da2-pkaf4mq6uzbthb73fh6shrafbm",
//   "aws_cognito_identity_pool_id": "ap-south-1:66657be0-5a5d-4d02-a0e7-2088b4c65768",
//   "aws_cognito_region": "ap-south-1",
//   "aws_user_pools_id": "ap-south-1_ftQXldhCl",
//   "aws_user_pools_web_client_id": "461n9phdr6hrnvj251o82ss8ob",
//   "oauth": {
//     "domain": "amplifyappa17d8077-a17d8077-dev.auth.ap-south-1.amazoncognito.com",
//     "scope": [
//         "email",
//     ],
//     "redirectSignIn": "http://localhost:3000/",
//     "redirectSignOut": "http://localhost:3000/",
//   },
//   "social": {
//     'FB': '660901318152990'
//   },
//   "federationTarget": "COGNITO_USER_POOLS",
// });



const loadFacebookSDK = () => {
  window.fbAsyncInit = function() {
    window.FB.init({
      appId            : '660901318152990',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
}

const waitForInit = () => {
  return new Promise((res, rej) => {
    const hasFbLoaded = () => {
      if (window.FB) {
        res();
      } else {
        setTimeout(hasFbLoaded, 300);
        console.log('waiting');
      }
    };
    hasFbLoaded();
  });
}

const poolData = {
  UserPoolId: 'ap-south-1_dKzSLzDJN', // Your user pool id here
  ClientId: '5kns1h0h9no1614dfrbko639g7', // Your client id here
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)

function App() {
  const [email, setEmail] = useState('swaps8087@gmail.com');
  const [password, setPassword] = useState('Password@123');
  const [otp, setOTP] = useState('');
  const [error, setError] = useState('');
  const [user, setUser] = useState({});
  const [todoName, setTodoName] = useState('');
  const [todo, setTodo] = useState('');
  const [cred, setCred] = useState({});

  useEffect(async () => {
    loadFacebookSDK();
    waitForInit();
    console.log(Auth);
    console.log(API);
    const creds = Auth.currentCredentials().then((e) => {
      console.log(e);
    });
    getCurrentUser();
    getData();
    console.log('getData');
  }, []);

  const getCurrentUser = async () => {
    const res = await Auth.currentSession()
    console.log(res);
    const cred = await Auth.currentCredentials();
    console.log(cred);
    const ucred = await Auth.currentUserCredentials();
    console.log(ucred);
    setCred(cred);
    setUser(res);
  };

  const getData = async () => {
    const res = await API.graphql(graphqlOperation(listTodos));
      console.log(res);
      console.log('got');
  };

  const handleSignUp = async () => {
    // try {
    //   const { user } = await Auth.signUp({
    //     username: email,
    //     password,
    //     'custom:role': 'tenants',
    //   });
    //   console.log(user);
    //   getCurrentUser();
    // } catch (err) {
    //     console.log('error signing up:', err);
    //     setError(err.message);
    // }
    try {
      const attributeList = [];
      attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({
        Name: 'email',
        Value: email,
      }));
      attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({
        Name: 'profile',
        Value: 'tenants',
      }));
      userPool.signUp(email, password, attributeList, null, (err, result) => {
        if(err) {
          console.log(err);
        }
        console.log(result);
        console.log('user name is ' + result.user.getUsername())
      });
    } catch (err) {
        console.log('error signing up:', err);
        setError(err.message);
    }
  };

  const handleVerify = async () => {
    try {
      const { user } = await Auth.confirmSignUp(email, otp);
      console.log(user);
      getCurrentUser();
    } catch (error) {
        console.log('error signing In:', error);
    }
  };

  const handleSignIn = async () => {
    try {
      const { user } = await Auth.signIn({
        username: email,
        password,

      });
      console.log(user);
      getCurrentUser();
      getData();
    } catch (err) {
        console.log('error signing In:', err);
        setError(err.message);
    }
  };

  const handleSignOut = async () => {
    try {
      const some = await Auth.signOut();
      console.log(some);
      localStorage.clear();
      setUser({});
    } catch (err) {
        console.log('error signing In:', err);
        setError(err.message);
    }
  };

  const handleForgotPassword = () => {
    Auth.forgotPassword(email)
      .then((e) => {
        console.log(e);
      });
  };
  const handleForgotPasswordSubmit = () => {
    Auth.forgotPasswordSubmit(email, otp, password)
      .then((e) => {
        console.log(e);
      });
  };

  const handleGoogleSignIn = () => {
    Auth.federatedSignIn();
  };


  const handleResponse = async (data) => {
    const { email, accessToken: token, expiresIn } = data;
    const expires_at = expiresIn * 1000 + new Date().getTime();
    const user = { email };

    // // this.setState({ isLoading: true });

    // try {
    //   const response = await Auth.federatedSignIn(
    //     "facebook",
    //     { token, expires_at },
    //     user
    //   );
    //   this.setState({ isLoading: false });
    //   // this.props.onLogin(response);
    // } catch (e) {
    //   this.setState({ isLoading: false });
    //   this.handleError(e);
    // }
    const res = await Auth.federatedSignIn(
      "facebook",
      {token, expires_at},
      user,
    )
    const re = (await Auth.currentSession()).getIdToken().getJwtToken();
    console.log(re);
  }

  const statusChangeCallback = response => {
    if (response.status === "connected") {
      handleResponse(response.authResponse);
    }
    console.log(response);
  };

  const checkLoginState = () => {
    window.FB.getLoginStatus(statusChangeCallback);
  };
  const handleFBSignIn = () => {
    window.FB.login(checkLoginState, {scope: "public_profile,email"});
  };


  const handleTodo = async () => {
    // const res = await API.graphql(graphqlOperation(createTodo, {input: {
    //   name: todoName,
    //   description: todo,
    // }}))
    // .then((e) => {
    //   console.log(e.data.createTodo);
    // });

    // const res = await API.graphql({
    //   query: createTodo,
    //   variables: {input: {
    //     name: todoName,
    //     description: todo,
    //   },
    //   authMode: 'AMAZON_COGNITO_USER_POOLS',
    //   }},
    //   {
    //     authorization: async () => (await Auth.currentSession()).getIdToken().getJwtToken(),
    //   })
    // .then((e) => {
    //   console.log(e.data.createTodo);
    // });
    console.log('request made');
    const res  = await client.mutate({
      mutation: gql(createTodo),
      variables: {
        input: {
          name: todoName,
          description: todo,
        }
      }})
    .then((e) => {
      console.log(e);
      console.log(e.data.createTodo);
    });
    console.log('request complete')
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <input placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
          <input placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
          <input placeholder="OTP" value={otp} onChange={(e) => setOTP(e.target.value)} />
          <button onClick={handleSignUp}>Sign Up</button>
          <button onClick={handleSignIn}>Sign In</button>
          <button onClick={handleVerify}>Verify</button>
          <button onClick={handleSignOut}>Sign Out</button>
          <button onClick={handleForgotPassword}>Forgot Password</button>
          <button onClick={handleForgotPasswordSubmit}>Forgot Password Submit</button>
        </p>
        <br />
        <p>
          <button onClick={handleGoogleSignIn}>Google Sign In</button>
          <button onClick={handleFBSignIn}>FB Sign In</button>
          <button onClick={() => Auth.federatedSignIn({provider: 'Google'})}>Open Google</button>
        </p>
        <p>{error}</p>
        <br />
        <p>
          <input placeholder="Todo Name" value={todoName} onChange={(e) => setTodoName(e.target.value)} />
          <input placeholder="Todo" value={todo} onChange={(e) => setTodo(e.target.value)} />
          <button onClick={handleTodo}>Add Todo</button>
        </p>
      </header>
    </div>
  );
}

export default App;




// {
//   "env": {
//     "browser": true,
//     "es6": true
//   },
//   "globals": {
//     "Atomics": "readonly",
//     "SharedArrayBuffer": "readonly"
//   },
//   "parser": "@typescript-eslint/parser",
//   "parserOptions": {
//     "project": "./tsconfig.json",
//     "ecmaFeatures": {
//       "jsx": true
//     },
//     "ecmaVersion": 2020,
//     "sourceType": "module"
//   },
//   "plugins": ["react", "@typescript-eslint", "prettier"],
//   "extends": [
//     "plugin:react/recommended",
//     "airbnb",
//     "plugin:prettier/recommended",
//     "plugin:@typescript-eslint/eslint-recommended",
//     "plugin:@typescript-eslint/recommended"
//   ],
//   "rules": {
//     "import/extensions": [
//       "error",
//       "ignorePackages",
//       {
//         "js": "never",
//         "jsx": "never",
//         "ts": "never",
//         "tsx": "never"
//       }
//     ],
//     "prettier/prettier": "error",
//     "@typescript-eslint/explicit-function-return-type": "off",
//     "@typescript-eslint/no-unused-vars": "off",
//     "react/jsx-filename-extension": [
//       1,
//       { "extensions": [".js", ".jsx", ".ts", ".tsx"] }
//     ]
//   },
//   "settings": {
//     "import/resolver": {
//       "node": {
//         "extensions": [".js", ".jsx", ".ts", ".tsx"]
//       }
//     }
//   }
// }





/*===============================================*/





import React, { useState, useEffect} from 'react';
import Amplify, { API, graphqlOperation, Auth, Hub } from 'aws-amplify';
import AWSAppSyncClient, { AUTH_TYPE } from 'aws-appsync';
import * as AmazonCognitoIdentity from 'amazon-cognito-identity-js';
import gql from 'graphql-tag';
import logo from './logo.svg';
import './App.css';
import { listTodos } from './graphql/queries';
import { createTodo } from './graphql/mutations';

const client = new AWSAppSyncClient({
  url: "https://relbel433zhwdnvoskbgbbqwny.appsync-api.ap-south-1.amazonaws.com/graphql",
  region: "ap-south-1",
  auth: {
    type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
    jwtToken: async () => (await Auth.currentSession()).getIdToken().getJwtToken(),
  },
});

const loadFacebookSDK = () => {
  window.fbAsyncInit = function() {
    window.FB.init({
      appId            : '660901318152990',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
}

const waitForInit = () => {
  return new Promise((res, rej) => {
    const hasFbLoaded = () => {
      if (window.FB) {
        res();
      } else {
        setTimeout(hasFbLoaded, 300);
        console.log('waiting');
      }
    };
    hasFbLoaded();
  });
}

const poolData = {
  UserPoolId: 'ap-south-1_dKzSLzDJN', // Your user pool id here
  ClientId: '5kns1h0h9no1614dfrbko639g7', // Your client id here
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)

function App() {
  const [email, setEmail] = useState('swaps8087@gmail.com');
  const [password, setPassword] = useState('Password@123');
  const [otp, setOTP] = useState('');
  const [error, setError] = useState('');
  const [user, setUser] = useState({});
  const [todoName, setTodoName] = useState('');
  const [todo, setTodo] = useState('');
  const [cred, setCred] = useState({});

  useEffect(async () => {
    loadFacebookSDK();
    waitForInit();
  }, []);

  const handleSignUp = async () => {
    try {
      const attributeList = [];
      attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({
        Name: 'email',
        Value: email,
      }));
      attributeList.push(new AmazonCognitoIdentity.CognitoUserAttribute({
        Name: 'profile',
        Value: 'tenants',
      }));
      userPool.signUp(email, password, attributeList, null, (err, result) => {
        if(err) {
          console.log(err);
        }
        console.log(result);
        console.log('user name is ' + result.user.getUsername())
      });
    } catch (err) {
        console.log('error signing up:', err);
        setError(err.message);
    }
  };

  const handleVerify = async () => {
    try {
      const { user } = await Auth.confirmSignUp(email, otp);
      console.log(user);
      // getCurrentUser();
    } catch (error) {
        console.log('error signing In:', error);
    }
  };

  const handleSignIn = async () => {
    try {
      const { user } = await Auth.signIn({
        username: email,
        password,

      });
      console.log(user);
      // getCurrentUser();
      // getData();
    } catch (err) {
        console.log('error signing In:', err);
        setError(err.message);
    }
  };

  const handleSignOut = async () => {
    try {
      const some = await Auth.signOut();
      console.log(some);
      localStorage.clear();
      setUser({});
    } catch (err) {
        console.log('error signing In:', err);
        setError(err.message);
    }
  };

  const handleForgotPassword = () => {
    Auth.forgotPassword(email)
      .then((e) => {
        console.log(e);
      });
  };
  const handleForgotPasswordSubmit = () => {
    Auth.forgotPasswordSubmit(email, otp, password)
      .then((e) => {
        console.log(e);
      });
  };

  const handleGoogleSignIn = () => {
    Auth.federatedSignIn();
  };


  const handleResponse = async (data) => {
    const { email, accessToken: token, expiresIn } = data;
    const expires_at = expiresIn * 1000 + new Date().getTime();
    const user = { email };

    // // this.setState({ isLoading: true });

    // try {
    //   const response = await Auth.federatedSignIn(
    //     "facebook",
    //     { token, expires_at },
    //     user
    //   );
    //   this.setState({ isLoading: false });
    //   // this.props.onLogin(response);
    // } catch (e) {
    //   this.setState({ isLoading: false });
    //   this.handleError(e);
    // }
    const res = await Auth.federatedSignIn(
      "facebook",
      {token, expires_at},
      user,
    )
    const re = (await Auth.currentSession()).getIdToken().getJwtToken();
    console.log(re);
  }

  const statusChangeCallback = response => {
    if (response.status === "connected") {
      handleResponse(response.authResponse);
    }
    console.log(response);
  };

  const checkLoginState = () => {
    window.FB.getLoginStatus(statusChangeCallback);
  };
  const handleFBSignIn = () => {
    window.FB.login(checkLoginState, {scope: "public_profile,email"});
  };


  const handleTodo = async () => {
    // const res = await API.graphql(graphqlOperation(createTodo, {input: {
    //   name: todoName,
    //   description: todo,
    // }}))
    // .then((e) => {
    //   console.log(e.data.createTodo);
    // });

    // const res = await API.graphql({
    //   query: createTodo,
    //   variables: {input: {
    //     name: todoName,
    //     description: todo,
    //   },
    //   authMode: 'AMAZON_COGNITO_USER_POOLS',
    //   }},
    //   {
    //     authorization: async () => (await Auth.currentSession()).getIdToken().getJwtToken(),
    //   })
    // .then((e) => {
    //   console.log(e.data.createTodo);
    // });
    console.log('request made');
    const res  = await client.mutate({
      mutation: gql(createTodo),
      variables: {
        input: {
          name: todoName,
          description: todo,
        }
      }})
    .then((e) => {
      console.log(e);
      console.log(e.data.createTodo);
    });
    console.log('request complete')
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <input placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
          <input placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
          <input placeholder="OTP" value={otp} onChange={(e) => setOTP(e.target.value)} />
          <button onClick={handleSignUp}>Sign Up</button>
          <button onClick={handleSignIn}>Sign In</button>
          <button onClick={handleVerify}>Verify</button>
          <button onClick={handleSignOut}>Sign Out</button>
          <button onClick={handleForgotPassword}>Forgot Password</button>
          <button onClick={handleForgotPasswordSubmit}>Forgot Password Submit</button>
        </p>
        <br />
        <p>
          <button onClick={handleGoogleSignIn}>Google Sign In</button>
          <button onClick={handleFBSignIn}>FB Sign In</button>
          <button onClick={() => Auth.federatedSignIn({provider: 'Google'})}>Open Google</button>
        </p>
        <p>{error}</p>
        <br />
        <p>
          <input placeholder="Todo Name" value={todoName} onChange={(e) => setTodoName(e.target.value)} />
          <input placeholder="Todo" value={todo} onChange={(e) => setTodo(e.target.value)} />
          <button onClick={handleTodo}>Add Todo</button>
        </p>
      </header>
    </div>
  );
}

export default App;

