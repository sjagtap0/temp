import React, { Component } from 'react';
import { Auth, Cache } from 'aws-amplify';

// To federated sign in from Google

class SignInWithGoogle extends Component {
    constructor(props) {
        super(props);
        this.signIn = this.signIn.bind(this);
    }

    componentDidMount() {
        const ga = window.gapi && window.gapi.auth2 ? 
            window.gapi.auth2.getAuthInstance() : 
            null;
        if (!ga) this.createScript();
    }

    signIn() {
        const ga = window.gapi.auth2.getAuthInstance();
        ga.signIn().then(
            googleUser => {
                this.getAWSCredentials(googleUser);
                // Run this after the sign-in
                const federatedInfo = Cache.getItem('federatedInfo');
                const { token } = federatedInfo;
                console.log(federatedInfo);
            },
            error => {
                console.log(error);
            }
        );
    }

   
    async signOut () {
        const ga = await window.gapi.auth2.getAuthInstance();
        if (ga != null) {
            ga.signOut().then(() => {
                ga.disconnect();
                console.log('disconnected');
            })
        }
        sessionStorage.clear();
    }

    async getAWSCredentials(googleUser) {
        const { id_token, expires_at } = googleUser.getAuthResponse();
        const some = googleUser.getAuthResponse();
        const profile = googleUser.getBasicProfile();
        let user = {
            email: profile.getEmail(),
            name: profile.getName()
        };

        const credentials = await Auth.federatedSignIn(
            'google',
            { token: id_token, expires_at },
            user
        ).then((e) => {
            console.log('user signed ins');
            this.props.getCurrentUser();
            console.log('credentials', e);
            localStorage.setItem('token', id_token);
            console.log(id_token);
        });
        
    }

    createScript() {
        // load the Google SDK
        const script = document.createElement('script');
        script.src = 'https://apis.google.com/js/platform.js';
        script.async = true;
        script.onload = this.initGapi;
        document.body.appendChild(script);
    }

    initGapi() {
        // init the Google SDK client
        const g = window.gapi;
        g.load('auth2', function() {
            g.auth2.init({
                client_id: '166001721536-7i213678setlm21i5v4n0lrllrht91p8.apps.googleusercontent.com',
                // authorized scopes
                scope: 'profile email'
            });
        });
    }

    render() {
        return (
            <div>
                <button onClick={this.signIn}>Sign in with Google</button>
                <button onClick={this.signOut}>Sign Out with Google</button>
            </div>
        );
    }
}

export default SignInWithGoogle;
