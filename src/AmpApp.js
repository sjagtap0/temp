import React, { useState, useEffect} from 'react';
import Amplify, { API, graphqlOperation, Auth, Hub } from 'aws-amplify';
import AWSAppSyncClient, { AUTH_TYPE } from 'aws-appsync';
// import * as AmazonCognitoIdentity from 'amazon-cognito-identity-js';
import {
  AuthenticationDetails,
  CognitoUserPool,
  CognitoUserAttribute,
  CognitoUser,
  CognitoUserSession,
} from "amazon-cognito-identity-js";
import gql from 'graphql-tag';
import { ApolloClient, InMemoryCache, HttpLink, ApolloProvider } from '@apollo/client';
import logo from './logo.svg';
import './App.css';
import { listTodos } from './graphql/queries';
import { createTodo } from './graphql/mutations';
import Calendar from './components/Calendar';

const poolData = {
  UserPoolId: 'ap-south-1_dKzSLzDJN', // Your user pool id here
  ClientId: '5kns1h0h9no1614dfrbko639g7', // Your client id here
};
const userPool = new CognitoUserPool(poolData);


const awsGraphqlFetch = async (uri, options) => {
  // const token = await window.localStorage.getItem('token');
  const curuser = await userPool.getCurrentUser();
  let token = '';
  const some = await curuser.getSession(async (err, session) => {
    console.log(session);
    token = session.getIdToken().getJwtToken();
    const validity = await session.isValid();
    console.log('session validity: ' + validity);
  });
  options.headers["Authorization"] = token;
  return fetch(uri, options);
};
// uri: "https://fxg2m8nwti.execute-api.us-east-1.amazonaws.com/dev/graphql"

const client = new ApolloClient({
  link: new HttpLink({ 
      uri: "https://tupz8ml7d1.execute-api.us-east-1.amazonaws.com/dev/graphql",
      fetch: awsGraphqlFetch
   }),
  cache: new InMemoryCache()
});

const loadFacebookSDK = () => {
  window.fbAsyncInit = function() {
    window.FB.init({
      appId            : '660901318152990',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
}

const waitForInit = () => {
  return new Promise((res, rej) => {
    const hasFbLoaded = () => {
      if (window.FB) {
        res();
      } else {
        setTimeout(hasFbLoaded, 300);
        console.log('waiting');
      }
    };
    hasFbLoaded();
  });
}


function App() {
  const [email, setEmail] = useState('swaps8087@gmail.com');
  const [password, setPassword] = useState('Password@123');
  const [otp, setOTP] = useState('');
  const [error, setError] = useState('');
  const [user, setUser] = useState({});
  const [todoName, setTodoName] = useState('');
  const [todo, setTodo] = useState('');
  const [cred, setCred] = useState({});

  useEffect(async () => {
    await loadFacebookSDK();
    await waitForInit();
    await getCurrentUser();
    // await getData();
  }, []);

  const getCurrentUser = async () => {
    console.log('getCurrentuser');

    const curuser = await userPool.getCurrentUser()
    const some = await curuser.getSession(async (err, session) => {
      // setUser(session);
      console.log(session);
      const validity = await session.isValid();
      console.log('session validity: ' + validity);
    });
    console.log(some);
  };

  const queryx = `query {
    notificationByUserId(userId: "user1") {
      id
      targetId
      userId
      content
      createAt
    }
  }`;

  const mutationx = `mutation {
    createNotification(
      input: { targetId: "de1", userId: "user1", content: "Hello World" }
    ) {
      id
    }
  }`;

  const getData = async () => {
    console.log('as---------------'); 

    // console.log(res);
    // const res  = await client.mutate({
    //   mutation: gql(mutationx)
    //   })
    // .then((e) => {
    //   console.log(e);
    //   // console.log(e.data.createTodo);
    // });
    //   console.log(res);
    //   console.log('got');
    const res = await client.query({
      query: gql(queryx)
    }).then((e) => {
      console.log(e);
    })

    console.log('|||---------------');
    // console.log('||SD---------------'); 

    // const ress = await API.graphql(graphqlOperation(queryx));
    // console.log(ress)   
    // console.log('||SD---------------'); 

  };

  const handleSignUp = async () => {
    try {
      const { user } = await Auth.signUp({
        username: email,
        password,
        'attributes': {
          'custom:role': 'tenants',
        }
      });
      console.log(user);
      getCurrentUser();
    } catch (err) {
        console.log('error signing up:', err);
        setError(err.message);
    }
  };

  const handleVerify = async () => {
    try {
      const { user } = await Auth.confirmSignUp(email, otp);
      console.log(user);
      getCurrentUser();
    } catch (error) {
        console.log('error signing In:', error);
    }
  };

  const handleSignIn = async () => {
    const authData = {
      Username: email,
      Password: password,
    };
    const authDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: email,
      Pool: userPool,
    };
    const cognitoUser = new CognitoUser(userData);
    cognitoUser.authenticateUser(authDetails, {
      onSuccess(result) {
        console.log('signed in')
        console.log(result);
        window.localStorage.setItem('token', result.idToken.jwtToken);
      },
      onFailure(err) {
        console.log(err);
      },
    });
    // getData();
  
  };

  const handleSignOut = async () => {
    const userData = {
      Username: email,
      Pool: userPool,
    };
    const cognitoUser = new CognitoUser(userData);

    const some = await cognitoUser.globalSignOut();
    console.log('signed out '+ some);
  };

  const handleForgotPassword = () => {
    Auth.forgotPassword(email)
      .then((e) => {
        console.log(e);
      });
  };
  const handleForgotPasswordSubmit = () => {
    Auth.forgotPasswordSubmit(email, otp, password)
      .then((e) => {
        console.log(e);
      });
  };

  const handleGoogleSignIn = () => {
    Auth.federatedSignIn();
  };


  const handleResponse = async (data) => {
    const { email, accessToken: token, expiresIn } = data;
    const expires_at = expiresIn * 1000 + new Date().getTime();
    const user = { email };
    // const res = await Auth.federatedSignIn(
    //   "facebook",
    //   {token, expires_at},
    //   user,
    // )
    // const re = (await Auth.currentSession()).getIdToken().getJwtToken();
    // console.log(re);
    const userData = {
      Username: email,
      Pool: userPool,
    };
    const cognitoUser = new CognitoUser(userData);

    const some = await cognitoUser.globalSignOut();

    const res = await cognitoUser.federatedSignIn(
      "facebook",
      {token, expires_at},
      user,
    )
    const other = await cognitoUser.getSession();
    console.log(other);
  }

  const statusChangeCallback = response => {
    if (response.status === "connected") {
      handleResponse(response.authResponse);
    }
    console.log(response);
  };

  const checkLoginState = () => {
    window.FB.getLoginStatus(statusChangeCallback);
  };
  const handleFBSignIn = () => {
    window.FB.login(checkLoginState, {scope: "public_profile,email"});
  };


  const handleTodo = async () => {
    console.log('request made');
    const res  = await client.mutate({
      mutation: gql(createTodo),
      variables: {
        input: {
          name: todoName,
          description: todo,
        }
      }})
    .then((e) => {
      console.log(e);
      console.log(e.data.createTodo);
    });
    console.log('request complete')
  };

  return (
    // <ApolloProvider client={client}>
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <input placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
          <input placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
          <input placeholder="OTP" value={otp} onChange={(e) => setOTP(e.target.value)} />
          <button onClick={handleSignUp}>Sign Up</button>
          <button onClick={handleSignIn}>Sign In</button>
          <button onClick={handleVerify}>Verify</button>
          <button onClick={handleSignOut}>Sign Out</button>
          <button onClick={handleForgotPassword}>Forgot Password</button>
          <button onClick={handleForgotPasswordSubmit}>Forgot Password Submit</button>
        </p>
        <br />
        <p>
          <button onClick={handleGoogleSignIn}>Google Sign In</button>
          <button onClick={handleFBSignIn}>FB Sign In</button>
          <button onClick={() => Auth.federatedSignIn({provider: 'Google'})}>Open Google</button>
        </p>
        <p>{error}</p>
        <br />
        <p>
          <input placeholder="Todo Name" value={todoName} onChange={(e) => setTodoName(e.target.value)} />
          <input placeholder="Todo" value={todo} onChange={(e) => setTodo(e.target.value)} />
          <button onClick={handleTodo}>Add Todo</button>
        </p>
      </header>
      {/* <Calendar /> */}
    </div>
    // </ApolloProvider>
  );
}

export default App;
