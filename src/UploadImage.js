import React, { useRef, useState } from 'react';
import { Storage, Auth } from 'aws-amplify';

const UploadImage = () => {
  const fileref = useRef(null);
  const [data, setData] = useState('');
  const [fileName, setFileName] = useState('');
  // useEffect({
  //   // const res = Storage.get('some.jpg');
  //   // console.log(res);
  //   // setData(res);
  // }, []);
  const handleUpload = async () => {
    const { identityId } = await Auth.currentCredentials();

    if (fileref.current && fileref.current.files) {
      Storage.put(`profilePic.jpg`, fileref.current.files[0], {
        level: 'protected',
        contentType: 'image/jpg',
      })
        .then((res) => {
          console.log('res', res);
        })
        .catch((err) => console.log('errr', err));
    }
  };

  const handleGet = async () => {
    console.log('get image');
    const { identityId } = await Auth.currentCredentials();
    console.log(identityId);
    const img = await Storage.get('profilePic.jpg', {
      level: 'protected',
      contentType: 'image/jpg',
    })
      .then((e) => {
        console.log('got itr', e);
        setData(e.toString());
      })
      .catch((e) => {
        console.log('errss', e);
      });
  };

  return (
    <div>
      <input type="file" ref={fileref} id="myFile" name="filename" />
      <button type="submit" onClick={handleUpload}>
        Upload
      </button>
      <input
        type="text"
        onChange={(e) => {
          setFileName(e.target.value);
        }}
      />
      <button type="submit" onClick={handleGet}>
        get image
      </button>
      <img src={data} alt="" style={{ width: '100px', height: '100px' }} />
    </div>
  );
};

export default UploadImage;
