import React, { useState } from 'react';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import timeGridPlugin from '@fullcalendar/timegrid';
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';

// import googleCalendarPlugin from '@fullcalendar/google-calendar';
// let calendar = new Calendar(calendarEl, {
//   plugins: [ resourceTimeGridPlugin ],
//   initialView: 'resourceTimeGridDay',
//   resources: [
//     // your list of resources
//   ]
// });
const FullCal = FullCalendar;

const Calendarx = () => {
  const [view, setView] = useState('timeGridDay');
  const handleDateClick = (e) => {
    console.log(e);
    // setView('listDay');
  };
  return (
    <div style={{height: '800px', width: '800px', margin: 'auto'}}>
      Calendar
      <FullCal
        plugins={[ dayGridPlugin, resourceTimelinePlugin,resourceTimeGridPlugin, listPlugin, interactionPlugin, timeGridPlugin]}
        // initialView='dayGridMonth'
        schedulerLicenseKey='CC-Attribution-NonCommercial-NoDerivatives'
        initialView={view}
        dateClick={handleDateClick}
        eventClick={handleDateClick}
        navLinks={true} // can click day/week names to navigate views
        
        selectable={true}    
  events={[
    {
      title: 'Meeting',
      start: '2020-11-02T14:30:00',
      extendedProps: {
        status: 'done'
      }
    },
    {
      title: 'Birthday Party',
      start: '2020-11-03T07:00:00',
      backgroundColor: 'green',
      borderColor: 'green',
    }
  ]}
  eventDidMount= { function(info) {
    if (info.event.extendedProps.status === 'done') {

      // Change background color of row
      info.el.style.backgroundColor = 'red';

      // Change color of dot marker
      var dotEl = info.el.getElementsByClassName('fc-event-dot')[0];
      if (dotEl) {
        dotEl.style.backgroundColor = 'white';
      }
    }
  }}
      />
    </div>
  )
};

export default Calendarx;
