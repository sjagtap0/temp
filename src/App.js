import React, { useState, useEffect} from 'react';
import Amplify, { API, graphqlOperation, Auth, Hub } from 'aws-amplify';
import { ApolloClient, InMemoryCache, HttpLink, ApolloProvider } from '@apollo/client';
import { AmplifyAuthenticator, AmplifySignOut, AmplifyGoogleButton } from '@aws-amplify/ui-react';
import gql from 'graphql-tag';
import logo from './logo.svg';
import './App.css';
import { listTodos } from './graphql/queries';
import { createTodo } from './graphql/mutations';
import Calendar from './components/Calendar';
import GoogleLogin from './GoogleLogin';
import FBLogin from './FBLogin';
import { Cache } from 'aws-amplify';
import UploadImage from './UploadImage';

const awsGraphqlFetch = async (uri, options) => {
  const token = await window.localStorage.getItem('token');
  options.headers["Authorization"] = token;
  // options.headers['Access-Control-Allow-Origin'] = '*';
  // options.mode='cors';
  return fetch(uri, options);
};

const client = new ApolloClient({
  link: new HttpLink({ 
    uri: "https://u4br8uszwf.execute-api.us-east-1.amazonaws.com/test/graphql",
    fetch: awsGraphqlFetch
   }),
  cache: new InMemoryCache()
});


// Amplify.configure({
//   "aws_project_region": "ap-south-1",
//   // "aws_appsync_graphqlEndpoint": "https://relbel433zhwdnvoskbgbbqwny.appsync-api.ap-south-1.amazonaws.com/graphql",
//   "aws_appsync_graphqlEndpoint": "https://u4br8uszwf.execute-api.us-east-1.amazonaws.com/test/graphql",
//   "aws_appsync_region": "us-east-1",
//   "aws_appsync_authenticationType": "AMAZON_COGNITO_USER_POOLS",
//   // "aws_appsync_apiKey": "da2-pkaf4mq6uzbthb73fh6shrafbm",
//   // "aws_cognito_identity_pool_id": "ap-south-1:66657be0-5a5d-4d02-a0e7-2088b4c65768",
//   // "aws_cognito_region": "ap-south-1",
//   // "aws_user_pools_id": "ap-south-1_ftQXldhCl",
//   // "aws_user_pools_web_client_id": "461n9phdr6hrnvj251o82ss8ob",
//   Auth: {

//     // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
//     identityPoolId: 'ap-south-1:9375cf25-363e-4a51-9f9d-6c19bf9616c4',

//     // REQUIRED - Amazon Cognito Region
//     region: 'ap-south-1',

//     // OPTIONAL - Amazon Cognito Federated Identity Pool Region 
//     // Required only if it's different from Amazon Cognito Region
//     // identityPoolRegion: 'XX-XXXX-X',

//     // OPTIONAL - Amazon Cognito User Pool ID
//     userPoolId: 'ap-south-1_vSUtYcrCL',

//     // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
//     userPoolWebClientId: '77foor7140uhctc7in5of3n061',

//     // OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
//     mandatorySignIn: false,

//     // OPTIONAL - Configuration for cookie storage
//     // Note: if the secure flag is set to true, then the cookie transmission requires a secure protocol
//     // cookieStorage: {
//     // // REQUIRED - Cookie domain (only required if cookieStorage is provided)
//     //     domain: '.yourdomain.com',
//     // // OPTIONAL - Cookie path
//     //     path: '/',
//     // // OPTIONAL - Cookie expiration in days
//     //     expires: 365,
//     // // OPTIONAL - See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite
//     //     sameSite: "strict" | "lax",
//     // // OPTIONAL - Cookie secure flag
//     // // Either true or false, indicating if the cookie transmission requires a secure protocol (https).
//     //     secure: true
//     // },

//     // OPTIONAL - customized storage object
//     // storage: MyStorage,

//     // OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
//     // authenticationFlowType: 'USER_PASSWORD_AUTH',

//     // OPTIONAL - Manually set key value pairs that can be passed to Cognito Lambda Triggers
//     // clientMetadata: { myCustomKey: 'myCustomValue' },

//      // OPTIONAL - Hosted UI configuration
//     oauth: {
//       "domain": "nbh-dev.auth.ap-south-1.amazoncognito.com",
//       "scope": [
//           "email",
//       ],
//       "redirectSignIn": "http://localhost:3000/",
//       "redirectSignOut": "http://localhost:3000/",
//       responseType: 'code' // or 'token', note that REFRESH token will only be generated when the responseType is code
//     },
//   },
//   // "oauth": {
//   //   "domain": "nbh-dev.auth.ap-south-1.amazoncognito.com",
//   //   "scope": [
//   //       "email",
//   //   ],
//   //   "redirectSignIn": "http://localhost:3000/",
//   //   "redirectSignOut": "http://localhost:3000/",
//   // },
//   // "federationTarget": "COGNITO_USER_POOLS",
//   API: {
//     graphql_headers: async () => {
//       // const session = await Auth.currentSession();
//       const token = await window.localStorage.getItem('token');
//       return {
//         // Authorization: session.getIdToken().getJwtToken(),
//         Authorization: token,
//       };
//     },
//   },
//   Storage: {
//     AWSS3: {
//       bucket: 'nbhdocbuc172025-dev',
//       region: 'ap-south-1',
//     }
//   }
//   // "aws_user_files_s3_bucket": "nbhdocbuc172025-dev",
//   // "aws_user_files_s3_bucket_region": "ap-south-1"
// });

Amplify.configure({
  "aws_project_region": "ap-south-1",
  // "aws_appsync_graphqlEndpoint": "https://u4br8uszwf.execute-api.us-east-1.amazonaws.com/test/graphql",
  // "aws_appsync_region": "us-east-1",
  // "aws_appsync_authenticationType": "AMAZON_COGNITO_USER_POOLS",
  "aws_cognito_identity_pool_id": "ap-south-1:c80a49b2-ac57-4537-8b8c-db07fb3824ba",
  "aws_cognito_region": "ap-south-1",
  "aws_user_pools_id": "ap-south-1_wTAhuQQ4S",
  "aws_user_pools_web_client_id": "1lh2ngt5kadqs7tcodjap338ct",
  "oauth": {
    "domain": "nbh-latest-dev.auth.ap-south-1.amazoncognito.com",
    "scope": [
        "email",
        "openid"
    ],
    "redirectSignIn": "https://master.d1sdwt2yq2gpsz.amplifyapp.com/",
    "redirectSignOut": "https://master.d1sdwt2yq2gpsz.amplifyapp.com/",
    "responseType": 'code' // or 'token', note that REFRESH token will only be generated when the responseType is code
  },
  Auth: {
    mandatorySignIn: false,
  },
  API: {
    graphql_headers: async () => {
      // const session = await Auth.currentSession();
      const token = await window.localStorage.getItem('token');
      return {
        // Authorization: session.getIdToken().getJwtToken(),
        Authorization: token,
      };
    },
  },
  "federationTarget": "COGNITO_USER_AND_IDENTITY_POOLS",
  Storage: {
    AWSS3: {
      bucket: 'neighbourhoodprofile142855-dev',
      region: 'ap-south-1',
      identityPoolId: 'ap-south-1:c80a49b2-ac57-4537-8b8c-db07fb3824ba',
    }
  }
});


function App(props) {
  const [email, setEmail] = useState('swaps8087@gmail.com');
  const [password, setPassword] = useState('Password@123');
  const [otp, setOTP] = useState('');
  const [error, setError] = useState('');
  const [user, setUser] = useState({});
  const [todoName, setTodoName] = useState('');
  const [todo, setTodo] = useState('');
  const [cred, setCred] = useState({});

  useEffect(async () => {
    console.log(props);
    await getCurrentUser();
    await getData();
  }, []);

  const getCurrentUser = async () => {
    console.log('----Current User----');
    const res = await Auth.currentSession()
    console.log(res);
    const cred = await Auth.currentCredentials();
    console.log(cred);
    const ucred = await Auth.currentUserCredentials();
    console.log(ucred);
    const authcur = await Auth.currentAuthenticatedUser();
    console.log(authcur);
    // Run this after the sign-in
    const federatedInfo = Cache.getItem('federatedInfo');
    console.log(federatedInfo);
    const session = await Auth.currentSession();
    localStorage.setItem('token', session.getIdToken().getJwtToken());
  };


  const queryx = `query {
    notificationByUserId(userId: "user1") {
      id
      targetId
      userId
      content
      createAt
    }
  }`;

  const mutationx = `mutation {
    createNotification(
      input: { targetId: "de1", userId: "user1", content: "Hello World" }
    ) {
      id
    }
  }`;
  
  const muta = `
  mutation {
    createUser(
      input: {  facebookID: "", googleID: "",
        email: "device@gmail.com", role: "Landlord",
        isEmailVerified: true, isPhoneVerified: false }
    ) {
      id
    }
  }
  `
  const createUser = `
  mutation {
    createUser(
      input: {}
    ) {
      id
      primaryEmail
      isEmailVerified
      isPhoneVerified
      role
    }
  }
  `;

  const getData = async () => {
    // console.log('Using Apollo Client');
    // const res = await client.query({
    //   query: gql(queryx)
    // });
    // console.log(res);

    // console.log('Using Apollo Client');
    // const res = await client.mutate({
    //   mutation: gql(createUser)
    // });
    // console.log(res);

    console.log('Using API from amplify');
    const ress = await API.graphql(graphqlOperation(createUser));
    console.log(ress)   

  };

  const handleSignUp = async () => {
    try {
      const { user } = await Auth.signUp({
        username: email,
        password
        // 'attributes': {
        //   'custom:role': 'landlords',
        // }
      });
      console.log(user);
      getCurrentUser();
    } catch (err) {
        console.log('error signing up:', err);
        setError(err.message);
    }
  };

  const handleVerify = async () => {
    try {
      const { user } = await Auth.confirmSignUp(email, otp);
      console.log(user);
      getCurrentUser();
    } catch (error) {
        console.log('error signing In:', error);
    }
  };

  const handleSignIn = async () => {
    try {
      const { user } = await Auth.signIn({
        username: email,
        password,
      });
      await getCurrentUser();
      await getData();
    } catch (err) {
      console.log('error signing In:', err);
      setError(err.message);
    }  
  };

  const handleSignOut = async () => {
    try {
      const some = await Auth.signOut();
      console.log(some);
      localStorage.clear();
      setUser({});
    } catch (err) {
        console.log('error signing In:', err);
        setError(err.message);
    }
    // window.FB.logout();
  };

  const handleForgotPassword = () => {
    Auth.forgotPassword(email)
      .then((e) => {
        console.log(e);
      });
  };
  const handleForgotPasswordSubmit = () => {
    Auth.forgotPasswordSubmit(email, otp, password)
      .then((e) => {
        console.log(e);
      });
  };

  const handleGoogleSignIn = () => {
    Auth.federatedSignIn({provider: 'Google', })
    .then(cred => {
      // If success, you will get the AWS credentials
      console.log(cred);
      return Auth.currentAuthenticatedUser();
    }).then(user => {
        // If success, the user object you passed in Auth.federatedSignIn
        console.log(user);
        getCurrentUser();
    }).catch(e => {
        console.log(e)
    });
  };


  // const handleResponse = async (data) => {
  //   const { email, accessToken: token, expiresIn } = data;
  //   const expires_at = expiresIn * 1000 + new Date().getTime();
  //   const user = { email };
  //   const res = await Auth.federatedSignIn(
  //     "facebook",
  //     {token, expires_at},
  //     user,
  //   )
  //   console.log(res);
  //   console.log("fb auth");
  //   const re = (await Auth.currentSession()).getIdToken().getJwtToken();
  //   console.log(re);
  // }

  // const statusChangeCallback = response => {
  //   if (response.status === "connected") {
  //     handleResponse(response.authResponse);
  //   }
  //   console.log(response);
  // };

  // const checkLoginState = () => {
  //   window.FB.getLoginStatus(statusChangeCallback);
  // };
  // const handleFBSignIn = () => {
  //   window.FB.login(checkLoginState, {scope: "public_profile,email"});
  // };


  const handleTodo = async () => {
    console.log('request made');
    // const res  = await client.mutate({
    //   mutation: gql(createTodo),
    //   variables: {
    //     input: {
    //       name: todoName,
    //       description: todo,
    //     }
    //   }})
    // .then((e) => {
    //   console.log(e);
    //   console.log(e.data.createTodo);
    // });
    console.log('request complete')
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <input placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
          <input placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
          <input placeholder="OTP" value={otp} onChange={(e) => setOTP(e.target.value)} />
          <button onClick={handleSignUp}>Sign Up</button>
          <button onClick={handleSignIn}>Sign In</button>
          <button onClick={handleVerify}>Verify</button>
          <button onClick={handleSignOut}>Sign Out</button>
          <button onClick={handleForgotPassword}>Forgot Password</button>
          <button onClick={handleForgotPasswordSubmit}>Forgot Password Submit</button>
        </p>
        <br />
        <p>
          {/* <button onClick={handleGoogleSignIn}>Google Sign In</button> */}
          {/* <button onClick={handleFBSignIn}>FB Sign In</button> */}
          {/* <button onClick={() => Auth.federatedSignIn({provider: 'Google'})}>Open Google</button> */}
          <GoogleLogin getCurrentUser={getCurrentUser}/>
          <FBLogin getCurrentUser={getCurrentUser}/>
          <button onClick={handleGoogleSignIn}>federated</button>
          <AmplifyAuthenticator>
            <div>
              My App
              <AmplifySignOut />
              <AmplifyGoogleButton />
            </div>
          </AmplifyAuthenticator>
        </p>
        <p>{error}</p>
        <br />
        <p>
          <input placeholder="Todo Name" value={todoName} onChange={(e) => setTodoName(e.target.value)} />
          <input placeholder="Todo" value={todo} onChange={(e) => setTodo(e.target.value)} />
          <button onClick={handleTodo}>Add Todo</button>
        </p>
        <br />
        <p>
          <UploadImage />
        </p>
      </header>
      {/* <Calendar /> */}
    </div>
  );
}

export default App;
